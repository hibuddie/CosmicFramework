<?php

namespace CosmicFramework\API;

class CosmicAccounts {

    const API_URL = "https://accounts.cosmic.media/api/v2";

    public static function StartAuthSession($private_key, $callback_url) {
        $callback_url = urlencode($callback_url);
        $response = file_get_contents(self::API_URL . "/start_auth_session?private_key=" . $private_key . "&callback_url=" . $callback_url);
        $response = json_decode($response, true);
        if($response["status"] == "error")
            return $response["error"];
        
        return $response["key"];
    }

    public static function FetchUserInfo($private_key, $token) {
        $response = file_get_contents(self::API_URL . "/fetch_user_info?private_key=" . $private_key . "&token=" . $token);
        $response = json_decode($response, true);
        if($response["status"] == "error")
            return $response["error"];

        return $response;
    }

    public static function CheckTokenValidation($token) {
        $response = file_get_contents(self::API_URL . "/check_token_validation?token=" . $token);
        $response = json_decode($response, true);
        if($response["status"] == "error")
            return $response["error"];

        return $response["validated"];
    }
}