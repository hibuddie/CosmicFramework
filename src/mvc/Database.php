<?php

namespace CosmicFramework\MVC;
use PDO;

class Database {
    static $connection_type;
    static $host;
    static $database;
    static $username;
    static $password;
    static $path;
    static $con;

    public static function init($params = null) {
        switch ($params['connection']) {
            case "mysql";
                self::$connection_type = $params['connection'];
                self::$host = $params['host'];
                self::$database = $params['database'];
                self::$username = $params['username'];
                self::$password = $params['password'];
                break;
            case "sqlite":
                self::$connection_type = $params['connection'];
                self::$path = $params['path'];
                break;
        }
        self::$con = self::connect();
    }

    private static function connect() {
        switch (self::$connection_type) {
            case "mysql":
                $pdo = new PDO('mysql:host=' . self::$host . ';dbname=' . self::$database . ';', self::$username, self::$password);
                break;
            case "sqlite":
                $pdo = new PDO('sqlite:' . self::$path);
                break;
        }
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT);
        return $pdo;
    }

    public static function query($query, $params = array()) {
        $statement = self::$con->prepare($query);
        $statement->execute($params);

        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        } elseif (explode(' ', $query)[0] == 'INSERT') {
            $last_user_id = self::$con->lastInsertId();
            if ($last_user_id !== 0) {
                return $last_user_id;
            }
        }
    }
    
    public static function MySQLStringConstructor($base_sql, $params = null){
        //SQL QUERY CONSTRUCTOR
        $sql = $base_sql;
        if (isset($params['order_by'])) {
            switch($params['order_by_clause']) {
                case "asc":
                    $list = "ASC";
                    break;
                case "desc":
                    $list = "DESC";
                    break;
            }
            $sql = $sql." ORDER BY `".$params['order_by']."` ".$list;
        } elseif (isset($params['order_by_cast_unsigned'])) {
            switch($params['order_by_clause']) {
                case "asc":
                    $list = "ASC";
                case "desc":
                    $list = "DESC";
            }
            $sql = $sql." ORDER BY CAST(".$params['order_by_cast_unsigned']." AS UNSIGNED ) ".$list;
        } elseif (isset($params['order_by_rand'])) {
            $sql = $sql." ORDER BY RAND()";
        }
        if (isset($params['limit'])) {
            $sql = $sql." LIMIT ".$params['limit'];
            if (isset($params['offset'])) {
                $sql = $sql." OFFSET ".$params['offset'];
            }
        }

        $sql = $sql.";";

        return $sql;
        //END
    }
}