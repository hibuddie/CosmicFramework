<?php
/*  View
*   The view object describes an application "view",
*   a to-be rendered page sent to the user.
*   It contains the template it will render,
*   and items to pass to the rendering engine to render
*/

namespace CosmicFramework\MVC;

use League\Plates\Engine as Renderer;
use ReflectionFunction;

class View {
    // $template, the template for the view to render
    private $template;
    // $items, items to pass to the template
    private $items;
    // $renderer, the object that will finally render the view
    private $renderer;

    public function __construct($template, $templates_path) {
        $this->template = $template;
        $this->renderer = new Renderer($templates_path);
    }

    public function setItems($items) {
        $this->items = $items;
    }

    public function registerFunction($function_name, $function) {
        $this->renderer->registerFunction($function_name, $function);
    }
    
    public function registerAllFunctions() {
        foreach (get_defined_functions(true)['user'] as $function_name) {
            if (!strstr($function_name, '\\')) {
                $f = new ReflectionFunction($function_name);
                $this->renderer->registerFunction($f->getName(), $f->getName());
            }
        }
    }

    // renders the template with class fields set in the constructor
    public function render() {
        // the renderer parses the template, then the view echos it's output
        echo $this->renderer->render($this->template, $this->items);
    }
}